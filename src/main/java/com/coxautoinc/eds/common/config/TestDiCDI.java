package com.coxautoinc.eds.common.config;

import com.coxautoinc.eds.common.DefaultValue;
import org.apache.deltaspike.cdise.api.CdiContainer;
import org.apache.deltaspike.cdise.api.CdiContainerLoader;
import org.apache.deltaspike.cdise.api.ContextControl;
import org.apache.deltaspike.core.api.config.ConfigProperty;
import org.apache.deltaspike.core.api.provider.BeanProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by akgillella on 7/22/16.
 */

@ApplicationScoped
public class TestDiCDI {

   private static final Logger logger = LoggerFactory.getLogger(TestDiCDI.class);

    @Inject
    @DefaultValue(key="test.user", value="user")
    private String user;

    @Inject
    @DefaultValue(key="test.password", value="pwd")
    private String password;

    @Inject
    @DefaultValue(key="test.password", value="100")
    private Integer intValue;





    public TestDiCDI() {
    }


    public void print(){

        System.out.println("Value of user: "+user);

        System.out.println("Value of password: "+password);

        System.out.println("Value of intValue: "+intValue);
    }


    public static void main(String[] args){

        // CDI container initialization
        CdiContainer cdiContainer = CdiContainerLoader.getCdiContainer();
        cdiContainer.boot();

        ContextControl contextControl = cdiContainer.getContextControl();
        contextControl.startContext(Singleton.class);
        contextControl.startContexts();

        //PropertyValueProducer p = BeanProvider.getContextualReference(PropertyValueProducer.class);

        TestDiCDI client = BeanProvider.getContextualReference(TestDiCDI.class);

        client.print();


        cdiContainer.shutdown();

//        Weld weld = new Weld();
//        WeldContainer container = weld.initialize();
//
//
//        DiCDI client = container.select(DiCDI.class).get();

    }


}

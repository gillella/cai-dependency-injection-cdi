package com.coxautoinc.eds.common.config;

import org.apache.commons.configuration.CombinedConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.DefaultConfigurationBuilder;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;
import java.io.File;

/**
 * Created by akgillella on 7/22/16.
 */

@Singleton
public class ConfigPropertyFinder {

    private static final Logger logger = LoggerFactory.getLogger(ConfigPropertyFinder.class);

    String configFile = "di-settings.xml";
    CombinedConfiguration config;

    @PostConstruct
    private void init() {
        try {
            String filePath = System.getProperty("config_file_path");
            if (StringUtils.isBlank(filePath)) {

                logger.info("you can supply an environment variable -Dconfig_file_path to JVM to provide a configuration properties injection settings file (default name is di-settings.xml) "
                        + " to read application specific config files ... below is the content of default di-settings.xml file " +
                        "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n" +
                        "<!-- Order of precedence is first to last -->\n" +
                        "<configuration>\n" +
                        "    <system />\n" +
                        "    <properties fileName=\"config.properties\" />\n" +
                        "</configuration>");


                logger.info("since config_file_path variable not supplied....loading default configuration properties injection settings from di-settings.xml if that file found in class path ");

                config = new DefaultConfigurationBuilder(configFile).getConfiguration(true);

            } else {

                logger.info("configuration properties injection settings are loaded from: {}", filePath);

                File configFile = new File(filePath);
                DefaultConfigurationBuilder builder = new DefaultConfigurationBuilder(configFile);
                config = builder.getConfiguration(true);

            }
        } catch (ConfigurationException configException) {
            logger.error("Error setting up ConfigPropertyFinder", configException.getCause());

        }


    }


    public String getValue(String key) {
        return config.getString(key);
    }

    public String[] getValues(String key) {
        return config.getStringArray(key);
    }



}


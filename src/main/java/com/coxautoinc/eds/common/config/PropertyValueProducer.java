package com.coxautoinc.eds.common.config;

import com.coxautoinc.eds.common.DefaultValue;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.deltaspike.core.api.config.ConfigProperty;
import org.apache.deltaspike.core.spi.config.BaseConfigPropertyProducer;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.regex.Pattern;

/**
 * Created by akgillella on 7/22/16.
 */

@ApplicationScoped
public class PropertyValueProducer{

    @Inject
    ConfigPropertyFinder propertyFinder;

    @Produces
    @Dependent
    @DefaultValue(key="key", value="value")
    public String getStringValue(InjectionPoint ip) {

        String value = null;

        String key = ip.getAnnotated().getAnnotation(DefaultValue.class).key();
        boolean isKeyDefined = !key.trim().isEmpty();
        if (isKeyDefined) {
            value = propertyFinder.getValue(key);
        }

        if (StringUtils.isNotEmpty(value)) {

            return value;

        }else {
            value = ip.getAnnotated().getAnnotation(DefaultValue.class).value();

            if (StringUtils.isNotEmpty(value)) {

                return value;

            } else {

                return null;
            }

        }
    }



    @Produces
    @Dependent
    @DefaultValue(key="key", value="value")
    public Float getFloatValue(InjectionPoint ip) {

        String value = getStringValue(ip);
        return (value != null) ? Float.valueOf(value) : null;
    }


    @Produces
    @Dependent
    @DefaultValue(key="key", value="value")
    public Double getDoubleValue(InjectionPoint ip) {

        String value = getStringValue(ip);
        return (value != null) ? Double.valueOf(value) : null;
    }


    @Produces
    @Dependent
    @DefaultValue(key="key", value="value")
    public Integer getIntegerValue(InjectionPoint ip) {

        String value = getStringValue(ip);
        return (value != null) ? Integer.valueOf(value) : null;
    }



    @Produces
    @Dependent
    @DefaultValue(key="key", value="value")
    public Long getLongValue(InjectionPoint ip) {

        String value = getStringValue(ip);
        return (value != null) ? Long.valueOf(value) : null;
    }


    @Produces
    @Dependent
    @DefaultValue(key="key", value="value")
    public Boolean getBooleanValue(InjectionPoint ip) {

        String value = getStringValue(ip);
        return (value != null) ? BooleanUtils.toBoolean(value.trim()) : null;
    }



    @Produces
    @Dependent
    @DefaultValue(key="key", value="value")
    public String[] getStringArrayValue(InjectionPoint ip) {

        String[] values = null;

        String key = ip.getAnnotated().getAnnotation(DefaultValue.class).key();
        boolean isKeyDefined = !key.trim().isEmpty();

        if (isKeyDefined) {
            values = propertyFinder.getValues(key);
        }

        if ((values == null || ArrayUtils.isEmpty(values))) {

            String defaultValue = ip.getAnnotated().getAnnotation(DefaultValue.class).value();

            if (defaultValue == null || defaultValue.trim().isEmpty()) {

                return null;

            } else {

                return Pattern.compile(",").split(defaultValue.trim());
            }

        }

        return values;
    }

}
